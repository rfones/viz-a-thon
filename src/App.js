import React, {
  Component
} from 'react';
import * as d3 from 'd3';
import './App.css';
import Hierarchy from './Hierarchy';

class App extends Component {
  state = {
    state: "New York",
    courts: []
  }

  componentWillMount() {
    d3.csv("/data/court_hierarchy.csv").then(data => {
      let courts = {}
      let count = 0;

      function newCourt(name) {
        return {
          id: count++,
          name: name,
          parentIds: [],
          childIds: []
        };
      }

      data.forEach(court => {
        // create state if it doesn't exist
        if (!courts[court.State]) {
          courts[court.State] = {};
        }

        // create new courts that don't exist
        if (!courts[court.State][court.ChildCourtName]) {
          courts[court.State][court.ChildCourtName] = newCourt(court.ChildCourtName);
        }
        if (!courts[court.State][court.ParentCourtName]) {
          courts[court.State][court.ParentCourtName] = newCourt(court.ParentCourtName);
        }

        // assign parent to child court
        courts[court.State][court.ChildCourtName].parentIds.push(courts[court.State][court.ParentCourtName]);

        // assign child to parent court
        courts[court.State][court.ParentCourtName].childIds.push(courts[court.State][court.ChildCourtName]);
      });

      this.restructureCourts(courts);
      console.log(courts);
      this.setState({
        'courts': courts
      });
    });
  }


  restructureCourts(json) {
    for (var stateName in json) {
      if (json.hasOwnProperty(stateName)) {
        var stateJson = json[stateName];
        var courts = [];

        for (var courtName in stateJson) {
          if (stateJson.hasOwnProperty(courtName)) {
            var courtJson = stateJson[courtName];
            courtJson.column = 0;
            courts.push(courtJson);
          }
        }

        var repeat = true;
        let maxColumns = 0;
        while (repeat) {
          repeat = false;
          courts.forEach(function (court) {
            var max = 0;
            court.parentIds.forEach(function (parent) {
              max = Math.max(max, parent.column);
            });

            max++;
            if (court.column < max) {
              court.column = max;
              maxColumns = Math.max(max, maxColumns)
              repeat = true;
            }
          });
        };

        // reverse court column order
        courts.forEach(court => {
          if (court.childIds.length === 0) {
            court.column = 1;
          } else {
            court.column = maxColumns - court.column + 1;
          }
        });

        // sort courts by name
        courts.sort((a, b) => a.name > b.name);

        // sort courts by first parent
        courts.sort((a, b) => {
          if (!a.parentIds[0] || !b.parentIds[0]) return -1;
          return a.parentIds[0].name > b.parentIds[0].name;
        })
      }
    }
  }

  changeState(event) {
    this.setState({state: event.target.value});
  }

  render() {
    if (this.state.courts.length === 0) return null;
    // let options = [];
    // for (let state in this.state.courts) {
    //   options.push(
    //     <option
    //           key={state}
    //           value={state}>
    //             {state}
    //         </option>
    //   )
    // }
    return (
      <div className = "App">
        <select value={this.state.state} onChange={this.changeState.bind(this)}>
          <option value="California">California</option>
          <option value="Georgia">Georgia</option>
          <option value="Iowa">Iowa</option>
          <option value="Maryland">Maryland</option>
          <option value="Massachusetts">Massachusetts</option>
          <option value="Montana">Montana</option>
          <option value="Nebraska">Nebraska</option>
          <option value="New York">New York</option>
          <option value="Oregon">Oregon</option>
          <option value="Washington">Washington</option>
        </select>
        <Hierarchy courts = { this.state.courts[this.state.state] }
        />

      </div>
    );
  }
}

export default App;