import React, { Component } from 'react';

export default class DrawLines extends Component {

    createPath(x1, y1, x2, y2) {
        var mx = (x1 + x2) / 2;
        return "M" + x1 + " " + y1 + "C" + mx + " " + y1 + "," + mx + " " + y2 + "," + x2 + " " + y2;
    }

    render() {
        if (this.props.courts === undefined) return null;
        let lines = [];
        this.props.courts.forEach(court => {
            if (court.childIds.length) {
                let x1 = court.x;
                let y1 = court.y + 25;
                court.childIds.forEach(child => {
                    let strokeColor = 'black';
                    let strokeWidth = 1;
                    let x2 = child.x + 250;
                    let y2 = child.y + 25;
                    let d = this.createPath(x1 - 10, y1, x2, y2);
                    let key = court.id + '_' + child.id;
                    if (this.props.selectedCourt == child.id) {
                        console.log('selected court', child.id);
                        strokeColor = 'blue'
                        strokeWidth = 3;
                    }
                    lines.push(
                        <path
                            key={key + '_path'}
                            d={d}
                            stroke={strokeColor}
                            strokeWidth={strokeWidth}
                            fill="none"
                            />
                    );
                });
                let fillColor = 'black';
                court.childIds.forEach(childCourt => {
                    if (childCourt.id === this.props.selectedCourt) {
                        fillColor = 'blue';
                    }
                })
                lines.push(
                    <polygon
                        key={court.id + '_poly'}
                        points="0,0 -30,-10 -25,0 -30,10"
                        style={{strokeWidth: 1}}
                        fill={fillColor}
                        transform={"translate(" + x1 + " " + y1 + ")scale(.5)"}
                        />
                );
            }
        });
        if (lines.length === 0) return null;
        return lines;
    }
}