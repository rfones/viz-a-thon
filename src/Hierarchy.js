import React, { Component } from 'react';
import * as d3 from 'd3';
import Court from './Court';
import DrawLines from './DrawLines';

export default class Hierarchy extends Component {
    state = {
        selectedCourt: null
    }

    selectCourt(courtId) {
        this.setState({'selectedCourt': courtId})
    }

    render() {
        if (this.props.courts === undefined) return null;
        // convert object to array
        let arr = Object.keys(this.props.courts).map((k) => this.props.courts[k])
        let rows = [0,0,0,0,0,0,0,0];
        arr.forEach(court => {
            rows[court.column] += Math.floor(court.childIds.length / 2) + 1;
            for (let i = court.column + 1; i < rows.length; i++) {
                if (rows[i] < rows[i-1] - 1) {
                    rows[i] += rows[i-1];
                }
            }
            court.x = 300 * (court.column - 1) + 10;
            court.y = (60 * (rows[court.column] - 1)) + 20;
        })
        return (
            <svg className="hierarchy"
                width="100%"
                height={1000} >
                <DrawLines
                    selectedCourt={this.state.selectedCourt}
                    courts={arr} />
                {arr.map(court => {
                    return (
                        <Court
                            key={court.id}
                            selectedCourt={this.state.selectedCourt}
                            court={court}
                            onClick={() => this.selectCourt(court.id)}
                            />
                    )
                })}
            </svg>
        )
    }
}