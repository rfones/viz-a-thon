import React, { Component } from 'react';

export default class Court extends Component {

    render() {
        if (this.props.court === undefined) return null;
        let { court } = this.props;
        let strokeColor = 'black';
        let strokeWidth = 1;
        if (this.props.selectedCourt === court.id) {
            strokeColor = 'blue';
            strokeWidth = 4;
        }
        return (
            <g className="court" onClick={() => this.props.onClick()}>
                <rect
                    id={"court" + court.id}
                    width={250}
                    height={50}
                    x={court.x}
                    y={court.y}
                    fill="#fff"
                    stroke={strokeColor}
                    strokeWidth={strokeWidth}
                    style={{font: 'sans-serif'}}
                    rx="15" ry="15">
                </rect>
                <text x={court.x + 10} y={court.y + 30}>{court.name}</text>
            </g>
        )
    }
}